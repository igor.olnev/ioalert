//
//  Alert+Banner.swift
//  AlertTest
//
//  Created by Igor Olnev on 12/08/2019.
//  Copyright © 2019 Igor Olnev. All rights reserved.
//

import UIKit

class BannerQueue {
    static let shared = BannerQueue()
    var queue: [IOBanner] = []
    internal var keyWindow: UIWindow? = nil

    var isQueue = true
    
    init() {
        keyWindow = UIApplication.shared.keyWindow
    }
    
    public func show(banner: IOBanner) {
        banner.action = popBanner
        queue.insert(banner, at: 0)
        if queue.count <= 1 {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                self.popBanner()
            }
        }
    }
    
    fileprivate func popBanner() -> () {
        guard !queue.isEmpty else {
            keyWindow?.windowLevel = UIWindow.Level.normal
            return
        }
        
        let banner = self.queue.removeLast()
        
        if let window = keyWindow {
            keyWindow?.windowLevel = UIWindow.Level.statusBar + 1
            window.makeKeyAndVisible()
            window.addSubview(banner)
            banner.draw()
        }
    }
}

class IOBanner: CustomView {
    // MARK: - Private properties
    private var textView = UITextView()
    
    
    
    override public class var preferredSize: CGSize {
        get {
            return CGSize(width: UIScreen.main.bounds.size.width, height: 0)
        }
    }
    
    // MARK: - Public properties
    fileprivate var action: (() -> ())?
    fileprivate var time: TimeInterval = 2
    fileprivate var text: String = "" {
        didSet {
            let components = text.components(separatedBy: .whitespacesAndNewlines)
            if components.count > 3 {
                time = Double(components.count/2)
            }
            self.textView.insertText(self.text)
        }
    }
    
    // MARK: - View setup
    override public func setup() {
        isUserInteractionEnabled = true
        clipsToBounds = true
        backgroundColor = UIColor(red: 40/255, green: 50/255, blue: 60/255, alpha: 1.0)

        textViewConfigure()
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(self.onSwipeAction))
        swipeUp.direction = .up
        self.addGestureRecognizer(swipeUp)
    }
    
    @objc private dynamic func onSwipeAction() {
        self.dismiss()
    }

    // Text
    /// - parameter text: Text message, duration is proportional by text lenght
    func show(text: String, isBlur: Bool = false) {
        self.text = text        
        BannerQueue.shared.show(banner: self)
    }
    
    fileprivate func draw(isBlure: Bool = false) {
        if isBlure {
            self.addBlur()
        }
        
        let width = UIScreen.main.bounds.size.width
        if let height = (self.textView.text?.heightWithConstrainedWidth(width, font: self.textView.font ?? UIFont())) {
            snp.makeConstraints { make in
                make.width.equalTo(width)
                make.topMargin.equalToSuperview()
                make.left.equalToSuperview()
                make.right.equalToSuperview()
            }
            
            present(width: width, height: height)
        }
    }
    
    fileprivate func present(width: CGFloat, height: CGFloat) {
        self.frame = CGRect(x: 0, y: -height, width: width, height: 0)
        self.fittingTest()

        UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.2, options: .curveEaseInOut, animations: {
            self.frame = CGRect(x: 0, y: 0, width: width, height: height)
        }, completion: { [weak self] _ in
            DispatchQueue.main.asyncAfter(deadline: .now() + (self?.time ?? 0.0), execute: {
                self?.dismiss()
            })
        })
    }
    
    fileprivate func dismiss() {
        UIView.animate(withDuration: 0.4, animations: {
            self.alpha = 0
        }, completion: { _ in
            self.removeFromSuperview()
            if let action = self.action { action() }
        })
    }
}

extension IOBanner {
    fileprivate func fittingTest() {
        textView.sizeToFit()
        addSubview(textView)
        self.textView.snp.makeConstraints { make in
            make.centerX.equalToSuperview()
            make.left.equalToSuperview().offset(10)
            make.right.equalToSuperview().offset(-10)
            make.topMargin.equalToSuperview().offset(10)
            make.bottom.equalToSuperview().offset(-10)
        }
    }
    
    fileprivate func addBlur() {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = bounds
        blurEffectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        addSubview(blurEffectView)
    }
    
    fileprivate func textViewConfigure() {
        textView.textColor = .white
        textView.backgroundColor = .clear
        textView.font = .boldSystemFont(ofSize: 13)
        textView.alpha = 0.8
        textView.textAlignment = .center
        textView.dataDetectorTypes = .all
        textView.isEditable = false
        textView.isMultipleTouchEnabled = true
        textView.isSelectable = true
        textView.isScrollEnabled = false
        textView.linkTextAttributes = [.foregroundColor: UIColor.lightGray]
    }
}

extension String {
    func widthWithConstrainedHeight(_ height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: height)

        let boundingBox = self.boundingRect(with: constraintRect,
                                            options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                            attributes: [NSAttributedString.Key.font: font],
                                            context: nil)

        return ceil(boundingBox.width)
    }

    func heightWithConstrainedWidth(_ width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect,
                                            options: NSStringDrawingOptions.usesLineFragmentOrigin,
                                            attributes: [NSAttributedString.Key.font: font],
                                            context: nil)

        return ceil(boundingBox.height)
    }
}
