// Igor Olnev 2019
// Renins

import UIKit

class AlertTextField: UITextField {
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 11, y: bounds.origin.y + 10.4, width: bounds.size.width - 10, height: bounds.size.height - 16);
    }

    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 14.2, y: bounds.origin.y + 10.4, width: bounds.size.width - 20, height: bounds.size.height - 16);
    }

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + 14.2, y: bounds.origin.y + 10.4, width: bounds.size.width - 20, height: bounds.size.height - 16);
    }
}
