// Igor Olnev 2019
// Renins

import UIKit

public class IOAlert: CustomView, UITextFieldDelegate {
    // MARK: - private properties

    private let buttonColor: UIColor = UIColor(red: 0.0, green: 122.0/255.0, blue: 1.0, alpha: 1.0)
    internal var keyWindow: UIWindow? = nil
    private(set) var style = Style.action
    
    lazy private(set) var screenBackground = UIView()
    private(set) var textField: AlertTextField? = nil
    
    // MARK: - Public properties
    
    public var height = 174
    public var cancelButtonAction: (() -> Void)?
    public var buttonAction: (() -> Void)?
    public var backgroundRadius: CGFloat = 13
    public var alertBackgroundColor: UIColor = .white
    
    public var alertSeparatorColor: UIColor? = .lightGray {
        didSet {
            buttonSeparator.backgroundColor = alertBackgroundColor
            buttonDivisor.backgroundColor = alertBackgroundColor
        }
    }
    
    public var buttonsHeight: CGFloat = 38
    
    public enum Style {
        case simple
        case action
    }
    
    public var titleFont: UIFont! {
        didSet {
            mainTextLabel.font = titleFont
        }
    }
    public var buttonsFont: UIFont! {
        didSet {
            actionButton.titleLabel?.font = buttonsFont
            cancelButton.titleLabel?.font = buttonsFont
        }
    }
    public var subtitleFont: UIFont! {
        didSet {
            secondaryTextLabel.font = subtitleFont
        }
    }
    
    override public class var preferredSize: CGSize {
        get {
            return UIScreen.main.bounds.size
        }
    }
    
    // MARK: - Outlets

    private(set) var background = UIView()
    
    public var mainTextLabel = UILabel()
    public var secondaryTextLabel = UILabel()
    
    public var cancelButton = UIButton()
    public var actionButton = UIButton()
    
    private(set) var buttonSeparator = UIView()
    private(set) var buttonDivisor = UIView()
    
    override public func setup() {
        keyWindow = UIApplication.shared.keyWindow
        
        frame = CGRect(origin: CGPoint.zero, size: type(of: self).preferredSize)
        
        var notificationFrame = self.bounds
        notificationFrame.origin.y += CGFloat(integerLiteral: height)
        
        self.frame = notificationFrame
        
        actionButton.titleLabel?.font = UIFont.systemFont(ofSize: 15)
        cancelButton.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
        
        mainTextLabel.font = UIFont.boldSystemFont(ofSize: 17)
        secondaryTextLabel.font = UIFont.systemFont(ofSize: 12)
        
        mainTextLabel.textColor = .black
        secondaryTextLabel.textColor = .black
        
        cancelButton.setTitleColor(buttonColor, for: .normal)
        actionButton.setTitleColor(buttonColor, for: .normal)
    }
}

// MARK: - Alert define
extension IOAlert {
    // Text
    /// Parameters descriptions for setting texts method
    ///
    /// - parameter withTitle: Alert title
    /// - parameter optional secondaryText: Alert subtitle
    /// - parameter optional actionButtonText: Action button title
    /// - parameter cancelButtonText: Cancel button title
    public func setText(withTitle titleText: String,
                        and secondaryText: String? = nil,
                        actionButtonText: String? = nil,
                        cancelButtonText: String) {
        if actionButtonText != nil  {
            style = .action
        } else {
            style = .simple
        }
        
        mainTextLabel.text = titleText
        secondaryTextLabel.text = secondaryText
        
//        actionButton = UIButton()
        actionButton.setTitle(actionButtonText, for: .normal)
        
//        cancelButton = UIButton()
        cancelButton.setTitle(cancelButtonText, for: .normal)
    }
    
    // Colors
    /// Parameters descriptions for setting colors method
    ///
    /// - parameter textColor: All texts color define
    /// - parameter cancelButtonColor: Cancel button textColor
    /// - parameter optional actionButtonColor: Action button color
    public func setColor(textColor: UIColor,
                         cancelButtonColor: UIColor,
                         actionButtonColor: UIColor? = nil) {
        mainTextLabel.textColor = textColor
        secondaryTextLabel.textColor = textColor
        
        cancelButton.setTitleColor(cancelButtonColor, for: .normal)
        if actionButtonColor != nil {
            actionButton.setTitleColor(actionButtonColor, for: .normal)
        }
    }
    
    // Fonts
    /// Parameters descriptions for setting fonts method
    ///
    /// - parameter titleFont: Title font
    /// - parameter buttonsFont: All buttons font
    public func setFont(titleFont: UIFont, buttonsFont: UIFont) {
        mainTextLabel.font = titleFont
        
        actionButton.titleLabel?.font = buttonsFont
        cancelButton.titleLabel?.font = buttonsFont
    }
    
    /// - parameter actionButtonFont: Action button font
    /// - parameter cancelButtonFont: Cancel button font
    // Buttons font
    public func setButtonsFont(actionButtonFont: UIFont,
                               cancelButtonFont: UIFont) {
        actionButton.titleLabel?.font = actionButtonFont
        cancelButton.titleLabel?.font = cancelButtonFont
    }
}

extension IOAlert {
    // Textfield
    public func addTextField(with placeholder: String,
                             textFieldFont: UIFont,
                             tintColor: UIColor,
                             bgColor: UIColor) {
        textField = AlertTextField()
        textField?.delegate = self
        
        height = 184
        textField?.placeholder = placeholder
        textField?.font = textFieldFont
        textField?.backgroundColor = bgColor
        textField?.tintColor = tintColor
    }
    
    public func show() {
        configure()
        self.alpha = 1
    }
    
    private func configure() {
        secondaryTextLabel.numberOfLines = 0
        secondaryTextLabel.sizeToFit()
        height += Int(secondaryTextLabel.bounds.size.height)
        
        addSubview(screenBackground)
        screenBackground.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        screenBackground.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        if let window = keyWindow { window.addSubview(self) }
        
        switch style {
        case .simple:
            simpleCompose()
            
        case .action:
            actionCompose()
        }
    }
    
    private func simpleCompose() {
        self.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        screenBackground.addSubview(background)
        background.backgroundColor = alertBackgroundColor
        background.layer.cornerRadius = backgroundRadius
        background.layer.masksToBounds = true
        background.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(53)
            make.right.equalToSuperview().offset(-53)
            make.centerY.equalToSuperview()
        }
        
        addSubview(mainTextLabel)
        mainTextLabel.textAlignment = NSTextAlignment.center
        mainTextLabel.snp.makeConstraints { make in
            make.left.equalTo(background.snp.left).offset(15)
            make.right.equalTo(background.snp.right).offset(-15)
            make.centerX.equalTo(background.snp.centerX).priority(ConstraintPriority(995.0))
            make.top.equalTo(background.snp.top).offset(30)
        }
        
        addSubview(secondaryTextLabel)
        mainTextLabel.textAlignment = NSTextAlignment.center
        secondaryTextLabel.textAlignment = NSTextAlignment.center
        secondaryTextLabel.snp.makeConstraints { make in
            make.left.equalTo(background.snp.left).offset(15)
            make.right.equalTo(background.snp.right).offset(-15)
            make.top.equalTo(mainTextLabel.snp.bottom).offset(5)
            make.bottom.equalTo(background.snp.bottom).offset(-buttonsHeight - 20)
        }
        
        addSubview(buttonSeparator)
        buttonSeparator.backgroundColor = UIColor(red: 217/255, green: 217/255, blue: 217/255, alpha: 0.5)
        buttonSeparator.snp.makeConstraints { make in
            make.width.equalTo(background.snp.width)
            make.height.equalTo(1)
            make.bottom.equalTo(background.snp.bottom).offset(-buttonsHeight)
            make.right.equalTo(background.snp.right)
            make.left.equalTo(background.snp.left)
        }
        
        background.addSubview(cancelButton)
        cancelButton.addTarget(self, action: #selector(cancelAction), for: .touchUpInside)
        cancelButton.snp.makeConstraints { make in
            make.left.equalTo(background.snp.left)
            make.right.equalTo(background.snp.right)
            make.height.equalTo(buttonsHeight)
            make.bottom.equalTo(background.snp.bottom)
        }
    }
    
    private func actionCompose() {
        self.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        screenBackground.addSubview(background)
        background.backgroundColor = alertBackgroundColor
        background.layer.cornerRadius = backgroundRadius
        background.layer.masksToBounds = true
        background.snp.makeConstraints { make in
            make.left.equalToSuperview().offset(53)
            make.right.equalToSuperview().offset(-53)
            make.centerY.equalToSuperview()
        }
        
        addSubview(mainTextLabel)
        mainTextLabel.textAlignment = NSTextAlignment.center
        mainTextLabel.snp.makeConstraints { make in
            make.left.equalTo(background.snp.left).offset(15)
            make.right.equalTo(background.snp.right).offset(-15)
            make.centerX.equalTo(background.snp.centerX).priority(ConstraintPriority(995.0))
            make.top.equalTo(background.snp.top).offset(20)
        }
        
        if let textField = self.textField {
            background.addSubview(textField)
            textField.textAlignment = NSTextAlignment.natural
            textField.layer.cornerRadius = backgroundRadius
            textField.snp.makeConstraints { make in
                make.left.equalTo(background.snp.left).offset(15)
                make.right.equalTo(background.snp.right).offset(-15)
                make.top.equalTo(mainTextLabel.snp.bottom).offset(5)
                make.bottom.equalTo(background.snp.bottom).offset(-buttonsHeight - 20)
            }
        } else {
            addSubview(secondaryTextLabel)
            mainTextLabel.textAlignment = NSTextAlignment.center
            secondaryTextLabel.textAlignment = NSTextAlignment.center
            secondaryTextLabel.snp.makeConstraints { make in
                make.left.equalTo(background.snp.left).offset(15)
                make.right.equalTo(background.snp.right).offset(-15)
                make.top.equalTo(mainTextLabel.snp.bottom).offset(5)
                make.bottom.equalTo(background.snp.bottom).offset(-buttonsHeight - 20)
            }
        }
        
        addSubview(buttonSeparator)
        buttonSeparator.backgroundColor = alertSeparatorColor
        buttonSeparator.snp.makeConstraints { make in
            make.width.equalTo(background.snp.width)
            make.height.equalTo(0.5)
            make.bottom.equalTo(background.snp.bottom).offset(-buttonsHeight)
            make.right.equalTo(background.snp.right)
            make.left.equalTo(background.snp.left)
        }
        
        background.addSubview(buttonDivisor)
        buttonDivisor.backgroundColor = alertSeparatorColor
        buttonDivisor.snp.makeConstraints { make in
            make.width.equalTo(0.5)
            make.top.equalTo(buttonSeparator.snp.bottom)
            make.bottom.equalTo(background.snp.bottom)
            make.centerX.equalToSuperview()
        }
        
        background.addSubview(cancelButton)
        cancelButton.addTarget(self, action: #selector(cancelAction), for: .touchUpInside)
        cancelButton.snp.makeConstraints { make in
            make.left.equalTo(background.snp.left)
            make.width.equalToSuperview().dividedBy(2)
            make.height.equalTo(buttonsHeight)
            make.bottom.equalTo(background.snp.bottom)
        }
        
        background.addSubview(actionButton)
        actionButton.addTarget(self, action: #selector(actionClick), for: .touchUpInside)
        actionButton.snp.makeConstraints { make in
            make.right.equalTo(background.snp.right)
            make.width.equalToSuperview().dividedBy(2)
            make.height.equalTo(buttonsHeight)
            make.bottom.equalTo(background.snp.bottom)
        }
    }
    
    // MARK: - Actions' methods
    
    @objc private func close() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseOut, animations: {
                self.alpha = 0
            }, completion: { finished in
                self.removeFromSuperview()
            })
        }
    }
    
    @objc private func cancelAction() {
        cancelButton.backgroundColor = UIColor.white.withAlphaComponent(0.7)

        UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseOut, animations: {
            self.cancelButton.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        }, completion: nil)
        
        if let action = cancelButtonAction {
            action()
        }
        
        close()
    }
    
    @objc public func actionClick() {
        actionButton.backgroundColor = UIColor.white.withAlphaComponent(0.4)
        
        UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseOut, animations: {
            self.actionButton.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        }, completion: nil)
        
        
        if let action = buttonAction {
            action()
        }
        
        close()
    }
    
    // MARK: Keyboard utilities
    
    @objc private func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            DispatchQueue.main.async {
                self.background.snp.updateConstraints { make in
                    make.centerY.equalToSuperview().offset(-(keyboardSize.height)/2.0)
                }
                
                UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseOut, animations: {
                    self.layoutIfNeeded()
                }, completion: nil)
            }
        }
    }
    
    @objc private func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            DispatchQueue.main.async {
                self.background.snp.updateConstraints { make in
                    make.centerY.equalToSuperview().offset(+(keyboardSize.height)/2.0)
                }
                
                UIView.animate(withDuration: 0.2, delay: 0.0, options: .curveEaseOut, animations: {
                    self.layoutIfNeeded()
                }, completion: nil)
            }
        }
    }
    
    public func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let action = buttonAction {
            action()
        }
        
        close()
        return false
    }
}

// Customs
open class CustomView: UIView {
    /// Override
    open class var preferredSize: CGSize {
        get {
            return CGSize.zero
        }
    }
    
    /// Override
    open func setup() {
        fatalError("Must override setup")
    }
    
    public convenience init() {
        self.init(frame: CGRect.zero)
        frame = CGRect(origin: CGPoint.zero, size: type(of: self).preferredSize)
        setup()
    }
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
}
