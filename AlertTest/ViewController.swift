import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var privet: UIImageView!
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        presentAlertOne()
        
//        let alertController = UIAlertController(title: "Выход", message: "Вы действительно хотите выйти?", preferredStyle: .alert)
//
//        let action1 = UIAlertAction(title: "Да", style: .default) { (action:UIAlertAction) in
//            print("You've pressed default");
//        }
//
//        let action2 = UIAlertAction(title: "Нет", style: .cancel) { (action:UIAlertAction) in
//            print("You've pressed cancel");
//        }
//
//        alertController.addAction(action1)
//        alertController.addAction(action2)
//        self.present(alertController, animated: true, completion: nil)
//
//
//        // Define init
//        let alert = Alert()
//        alert.setText(withTitle: "Выход",
//                      and: "Вы действительно хотите выйти?",
//                      actionButtonText: "Да",
//                      cancelButtonText: "Нет")
//
//        alert.buttonAction = {
//            debugPrint("Action")
//            self.presentBanners()
//
//        }
//
//        alert.cancelButtonAction = {
//            debugPrint("Cancel")
//            self.presentBanners()
//        }
//
//        // Show
//        alert.show()
        
        presentBanners()
    }

    var window: UIWindow? = nil
    
    func presentAlertOne() {
        // Define init
        let alert = IOAlert()
        alert.setText(withTitle: "Выход",
                      cancelButtonText: "OK")
        
//        alert.cancelButtonAction = {
//            debugPrint("Cancel")
//            self.presentAlertTwo()
//        }
        
//        alert.setColor(textColor: .black,
//                       cancelButtonColor: .purple,
//                       actionButtonColor: .purple)
        
//        // Define custom font
//        alert.titleFont = UIFont.boldSystemFont(ofSize: 15)
//        alert.subtitleFont = UIFont.systemFont(ofSize: 12)
//
//        alert.setButtonsFont(actionButtonFont: UIFont.boldSystemFont(ofSize: 15), cancelButtonFont: UIFont.systemFont(ofSize: 15))
        
        // Show
        alert.show()
    }
    
    func presentAlertTwo() {
        // Define init
        let alert = IOAlert()
        alert.setText(withTitle: "Выход", and:"Вы действительно хотите выйти?", cancelButtonText: "OK")
        alert.cancelButtonAction = {
            debugPrint("Cancel")
            self.presentAlertTwoAction()
        }
        
        alert.setColor(textColor: .black,
                       cancelButtonColor: .purple,
                       actionButtonColor: .purple)
        
        // Define custom font
        alert.titleFont = UIFont.boldSystemFont(ofSize: 15)
        alert.subtitleFont = UIFont.systemFont(ofSize: 12)
        
        alert.setButtonsFont(actionButtonFont: UIFont.boldSystemFont(ofSize: 15), cancelButtonFont: UIFont.systemFont(ofSize: 15))
        
        // Show
        alert.show()
    }
    
    func presentAlertTwoAction() {
        // Define init
        let alert = IOAlert()
        alert.show()
        alert.setText(withTitle: "Выход",
                      and: "Вы действительно хотите выйти?",
                      actionButtonText: "Да",
                      cancelButtonText: "Нет")
        alert.buttonAction = {
            debugPrint("Action")
            self.presentAlertTextfield()
        }
        alert.cancelButtonAction = {
            debugPrint("Cancel")
        }
        
        alert.setColor(textColor: .black,
                       cancelButtonColor: .purple,
                       actionButtonColor: .purple)
        
        // Define custom font
        alert.titleFont = UIFont.boldSystemFont(ofSize: 15)
        alert.subtitleFont = UIFont.systemFont(ofSize: 12)
        
        alert.setButtonsFont(actionButtonFont: UIFont.boldSystemFont(ofSize: 15), cancelButtonFont: UIFont.systemFont(ofSize: 15))
        
        // Show
        alert.show()
    }
    
    func presentAlertTextfield() {
        // Define init
        let alert = IOAlert()
        alert.show()
        alert.setText(withTitle: "Выход",
                      actionButtonText: "Да",
                      cancelButtonText: "Нет")
        alert.buttonAction = {
            debugPrint("Action")
            self.presentBanners()
        }
        
        alert.cancelButtonAction = {debugPrint("Cancel")}
        
        alert.setColor(textColor: .black,
                       cancelButtonColor: .purple,
                       actionButtonColor: .purple)
        alert.addTextField(with: "Placeholder text",
                           textFieldFont: UIFont.boldSystemFont(ofSize: 13),
                           tintColor: UIColor.lightGray,
                           bgColor: UIColor.groupTableViewBackground)
        
        // Define custom font
        alert.titleFont = UIFont.boldSystemFont(ofSize: 15)
        alert.subtitleFont = UIFont.systemFont(ofSize: 12)
        alert.setButtonsFont(actionButtonFont: UIFont.boldSystemFont(ofSize: 15), cancelButtonFont: UIFont.systemFont(ofSize: 15))
        
        // Show
        alert.show()
    }
    
    func presentBanners() {
        IOBanner().show(text: "Привет это текст для тестирование 1")
        IOBanner().show(text: "Привет это текст для тестирование 2 www.test.com")
        IOBanner().show(text: "Привет это текст для тестирование 3 +1 (234) 567890")
    }
}
