# Options
* Single title option
* Subtitle text option
* Single action option
* Submit / Cancel actions option


# How to create an IOAlert

## 1 Create alert
```swift
let alert = IOAlert()
```

## 1.1 Create a banner alert
```swift
IOBanner().show(text: "Text")
```

## 2 Define alert with characteristics
### 2.0.1 Single action option
```swift
alert.setText(with: "Title", and: "Subtitle" /*optional*/, cancelButtonText: "OK")
alert.cancelButtonAction = {}
```

### 2.0.2 Two actions option
```swift
alert.setText(with: "Title", and: "Subtitle" /*optional*/, actionButtonText: "YЕS" /*optional*/, cancelButtonText: "NO")
alert.buttonAction = {}
```

## 2.1 Color
```swift
alert.setColor(textColor: .black, cancelButtonColor: .purple, actionButtonColor: .purple)
```

## 2.2 Font
### Title
```swift
alert.titleFont = UIFont.systemFont(ofSize: 16)
alert.subtitleFont = UIFont.systemFont(ofSize: 13)
```

### Buttons
```swift
alert.setButtonsFont(actionButtonFont: UIFont.boldSystemFont(ofSize: 15), cancelButtonFont: UIFont.systemFont(ofSize: 15))

// All buttons' custom
alert.buttonsFont = UIFont.boldSystemFont(ofSize: 13)
```

## 2.3 Textfield
### Only two actions options
```swift
alert.addTextField(with: "Placeholder text", textFieldFont: UIFont.boldSystemFont(ofSize: 13), tintColor: UIColor.lightGray, bgColor: UIColor.groupTableViewBackground)
```


## 3.1 Present on view
```swift
alert.show()
```
